import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

import { ConfigProvider, theme } from 'antd';

const { darkAlgorithm, defaultAlgorithm } = theme;

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
	// <React.StrictMode>
	<ConfigProvider
		// theme={{
		// 	algorithm: defaultAlgorithm,
		// }}
		theme={{
			token: {
				// colorPrimary: '#00b96b',
			},
			components: {
				List: {
					contentWidth:500
					// colorPrimary: '#00b96b',
					// algorithm: true, // Enable algorithm
				},
			},
		}}
	>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</ConfigProvider>
	// </React.StrictMode>
);
