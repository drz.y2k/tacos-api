import {  useParams } from 'react-router-dom';

export const MocktailDetail = () => {
	const { variant, food } = useParams();

	return (
		<div>
			MocktailDetail {food}
			{variant}
		</div>
	);
};
