import { Card, Layout, Col, Row } from 'antd';
import { Link } from 'react-router-dom';

const { Header, Content } = Layout;

const foods = [
	{
		name: 'Tacos',
		image: 'foods/tacos.jpg',
	},
	{
		name: 'Burritos',
		image: 'foods/burritos.jpg',
	},
];

export const ListTacos = () => {
	return (
		<>
			{foods.map(({ name, image }) => {
				return (
					<Col xs={24} sm={12} md={8} lg={4} key={name}>
						<Card
							style={{
								height: '200px',
							}}
							hoverable
							cover={
								<Link to={`/${name.toLowerCase()}`}>
									<div
										style={{
											overflow: 'hidden',
											height: '200px',
											objectFit: 'cover',
											backgroundImage: `url(${image})`,
											backgroundPosition: 'center',
											backgroundSize: 'cover',
											borderRadius: '10px',
										}}
									></div>
								</Link>
							}
						>
							<Card.Meta title={name} />
						</Card>
					</Col>
				);
			})}
		</>
	);
};
