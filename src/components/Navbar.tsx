import { Image } from 'antd';

export const Navbar: React.FC = () => {
	return (
		<div
			style={{
				
				position: 'sticky',
				top: 0,
				backgroundColor: 'white',
				height: '40px',
				display: 'flex',
				alignItems: 'center',
				padding: '10px',
				fontWeight: 'bold',
				fontSize: '1.3rem',
				zIndex:2,
				boxShadow:'0px 0px 4px 0px rgba(0,0,0,0.75)'
			}}
		>
			<Image src="yellow_mocktail.png" preview={false} width={35} />
			Moockle
		</div>
	);
};
