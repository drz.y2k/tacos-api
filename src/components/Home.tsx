import { Card, Layout, Col, Row, Button, Skeleton } from 'antd';
import { useEffect, useState } from 'react';
import { getCocktails } from '../api/apiCocktails';
import { Link } from 'react-router-dom';
import { Loader } from './Loader';

import { RandomMocktailDisplay } from './RandomMocktailDisplay';

import { cocktail } from '../interface/cocktailList.interface';
import { useStore } from '../store/store';

// interface RandomMocktailProps {
// 	mocktailsList: cocktail[];
// }

const RandomMocktailTile: React.FC = () => {
	const { getRandomMocktail } = useStore((state) => state);

	return (
		<Col
			xs={24}
			sm={12}
			md={8}
			lg={6}
			xl={4}
			// style={{ padding: '10px' }}
		>
			<Card
				onClick={() => getRandomMocktail()}
				style={{
					height: '100%',
				}}
				hoverable
				cover={
					<div
						style={{
							overflow: 'hidden',
							height: '200px',
							objectFit: 'cover',
							backgroundImage: `url('question-mark.jpg')`,
							backgroundPosition: 'center',
							backgroundSize: 'cover',
							borderRadius: '10px',
						}}
					></div>
				}
			>
				<Card.Meta title="Random Cocktail" />
				<Card.Meta title={`?`} style={{ opacity: '0.5' }} />
			</Card>
		</Col>
	);
};

export const Home: React.FC = () => {
	const { modifyFullList, mocktailsFullList } = useStore((state) => state);

	useEffect(() => {
		getCocktails().then((data) => {
			modifyFullList(data);
		});
	}, []);

	return (
		<>
			{mocktailsFullList.length == 0 ? (
				<Loader message="Loading mocktails..." />
			) : (
				<>
					<RandomMocktailDisplay />
					<Row
						gutter={[15, 15]}
						// gutter={{ xs: 8, sm: 16, md: 24, lg: [32,32] }}
						// align={'middle'}
					>
						<RandomMocktailTile />
						{mocktailsFullList.map(
							({ strDrink, strDrinkThumb, idDrink }) => {
								return (
									<Col
										xs={24}
										sm={12}
										md={8}
										lg={6}
										xl={4}
										key={idDrink}
										// style={{ padding: '10px' }}
									>
										<Link to={`/${idDrink}`}>
											<Card
												style={{
													height: '100%',
												}}
												hoverable
												cover={
													<div
														style={{
															overflow: 'hidden',
															height: '200px',
															objectFit: 'cover',
															backgroundImage: `url(${strDrinkThumb})`,
															backgroundPosition:
																'center',
															backgroundSize:
																'cover',
															borderRadius:
																'10px',
														}}
													></div>
												}
											>
												<Card.Meta title={strDrink} />
												<Card.Meta
													title={`ID: ${idDrink}`}
													style={{ opacity: '0.5' }}
												/>
											</Card>
										</Link>
									</Col>
								);
							}
						)}
					</Row>
				</>
			)}
		</>
	);
};
