import { Spin, Button } from 'antd';
// import { LoadingOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { FC } from 'react';

interface LoaderProps {
	message: string;
	button?: boolean;
}

export const Loader: FC<LoaderProps> = ({ message, button }) => {
	return (
		<>
			<div
				style={{
					padding: '50px',
					backgroundColor: 'white',
					borderRadius: '10px',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'center',
					gap: '10px',
				}}
			>
				<Spin
					style={{ fontSize: '25px', fontWeight: 'bold' }}
					size="large"
					tip={message}
				/>
				{button && (
					<Link
						to={'/'}
						style={{ display: 'flex', justifyContent: 'center' }}
					>
						<Button
							type="primary"
							style={{
								fontSize: '15px',
								height: '40px',
								width: '500px',
							}}
						>
							👈🏻 Go home
						</Button>
					</Link>
				)}
			</div>
		</>
	);
};
