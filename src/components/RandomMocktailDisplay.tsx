import React, { useState } from 'react';
import { motion, AnimatePresence } from 'framer-motion';
import { Row, Card, Button, Grid, Col } from 'antd';
const { useBreakpoint } = Grid;

import { Link } from 'react-router-dom';

import { useStore } from '../store/store';

const variants = {
	initial: {
		opacity: 0,
		height: 0,
		transition: { duration: 0.3 },
	},
	animate: {
		opacity: 1,
		height: 'auto',
		transition: { duration: 0.3 },
	},
	exit: {
		opacity: 0,
		height: 0,
		transition: { duration: 0.3 },
	},
	// transition:{ duration: 0.5 }
	// rotate: { rotate: [0, -30, 0], transition: { duration: 0.5 } },
	// stop: { y: [0, -10, 0], transition: { repeat: Infinity, repeatDelay: 3 } },
};

export const RandomMocktailDisplay: React.FC = () => {
	const {
		singleMocktailSelected: mocktail,
		getRandomMocktail,
		activatedRandomMocktail,
	} = useStore((state) => state);

	const backgroundMocktailURL =
		mocktail?.strDrinkThumb != undefined
			? `url(${mocktail?.strDrinkThumb})`
			: `url('question-mark.jpg')`;

	const screens = useBreakpoint();

	const breakButton = screens.md;

	return (
		<Row>
			<AnimatePresence>
				{activatedRandomMocktail && (
					<motion.div
						variants={variants}
						initial="initial"
						animate="animate" // Descomenta esta línea
						exit="exit"
						style={{
							padding: '20px',
							backgroundColor: '#ffc95b',
							width: '100%',
							borderRadius: '10px',
						}}
					>
						<Card bordered={true}>
							<Row>
								<Col
									xs={24}
									style={{
										textAlign: 'center',
										fontWeight: 'bold',
										fontSize: '30px',
									}}
								>
									Random Mocktail Selector
								</Col>

								<Col
									xs={24}
									sm={24}
									md={12}
									lg={12}
									style={{
										padding: '10px',
										textAlign: 'center',
									}}
								>
									<div
										style={{
											fontSize: '22px',
											fontWeight: 'bold',
										}}
									>
										{mocktail?.strDrink ??
											'Random Mocktail'}
									</div>
									<div
										style={{
											backgroundImage:
												backgroundMocktailURL,
											height: '300px',
											margin: 'auto',
											maxWidth: '400px',
											backgroundSize: 'cover',
											overflow: 'hidden',
											backgroundPosition: 'center',
											borderRadius: '10px',
										}}
									></div>
								</Col>
								<Col
									xs={24}
									sm={24}
									md={12}
									lg={12}
									style={{
										display: 'flex',
										flexDirection: 'column',
										justifyContent: 'space-around',
										padding: '10px',
										gap: '10px',
									}}
								>
									<Link to={`/${mocktail?.idDrink}`}>
										<Button
											type="primary"
											style={{
												fontSize: '24px',
												height: breakButton
													? '50px'
													: '100px',
											}}
											block
										>
											Go to mocktail {!breakButton ? <br/>:' '} detail
										</Button>
									</Link>
									<Button
										onClick={() => {
											getRandomMocktail(0);
										}}
										type="primary"
										style={{
											fontSize: '25px',
											height: '50px',
										}}
										block
									>
										🎲 Try again
									</Button>
								</Col>
							</Row>
						</Card>
					</motion.div>
				)}
			</AnimatePresence>
		</Row>
	);
};
