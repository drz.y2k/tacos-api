import { Link, Outlet, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getCocktail } from '../api/apiCocktails';
import { Image, Button, Descriptions, Row, Col, Grid } from 'antd';
const { useBreakpoint } = Grid;
import { Loader } from './Loader';
import { Navbar } from './Navbar';
import { cocktail } from '../interface/cocktail.interface';

const MAX_INGREDIENTS = 14;

export const MocktailsMain = () => {
	const screens = useBreakpoint();

	const { idCocktail } = useParams();

	if (!idCocktail) return <div>Not found</div>;

	const [details, setDetails] = useState<cocktail>({} as cocktail);

	const ingredientsAndPortions = (): string[] => {
		if (!details) return [];
		let ingredients: string[] = [];
		for (let i: number = 1; i <= MAX_INGREDIENTS; i++) {
			const keyIngredient = `strIngredient${i}`;
			const keyMeasure = `strMeasure${i}`;

			if (keyIngredient in details && keyMeasure in details) {
				const ingredient = details[keyIngredient as keyof cocktail];
				const measure = details[keyMeasure as keyof cocktail];
				if (ingredient && measure) {
					ingredients.push(`${measure} ${ingredient}`);
				}
			}
		}
		return ingredients;
	};

	ingredientsAndPortions();

	useEffect(() => {
		getCocktail(idCocktail).then((data) => {
			setDetails(data);
		});
	}, []);

	return (
		<>
			<Navbar/>
			<div
				style={{
					backgroundColor: 'white',
					padding: '30px',
					margin: '30px',
					borderRadius: '25px',
				}}
			>
				{!Object.keys(details).length ? (
					<Loader message="loading mocktail info..." button />
				) : (
					<Row>
						<Col
							xs={24}
							lg={8}
							style={{
								display: 'flex',
								flexDirection: 'column',
								alignItems: 'center',
								gap: '10px',
							}}
						>
							<Image width={'90%'} src={details.strDrinkThumb} />
							<Link to={'/'}>
								<Button
									type="primary"
									style={{ fontSize: '20px', height: '50px' }}
								>
									👈🏻 Go home
								</Button>
							</Link>
						</Col>
						<Col xs={24} lg={16}>
							<Descriptions
								bordered={true}
								column={{ xs: 1, sm: 1, md: 2 }}
								layout={!screens.md ? 'vertical' : 'horizontal'}
							>
								<Descriptions.Item
									label="Name"
									style={{ fontSize: '20px' }}
								>
									{details?.strDrink}
								</Descriptions.Item>
								<Descriptions.Item
									label="Category"
									style={{ fontSize: '20px' }}
								>
									{details?.strCategory}
								</Descriptions.Item>
								<Descriptions.Item
									label="Ingredients"
									span={2}
									style={{ fontSize: '20px' }}
								>
									<ul>
										{ingredientsAndPortions().map(
											(ingredient) => (
												<li key={ingredient}>
													{ingredient}
												</li>
											)
										)}
									</ul>
								</Descriptions.Item>
								<Descriptions.Item
									label="Instructions"
									style={{ fontSize: '20px' }}
									span={2}
								>
									{details?.strInstructions}
								</Descriptions.Item>
							</Descriptions>
						</Col>
					</Row>
				)}
			</div>
		</>
	);
};
