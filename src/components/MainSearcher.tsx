import {
	Card,
	Layout,
	Col,
	Row,
	Button,
	Skeleton,
	Typography,
	Input,
	Image,
	List,
} from 'antd';
const { Title } = Typography;
import { useStore } from '../store/store';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { getCocktails } from '../api/apiCocktails';
import { cocktail } from '../interface/cocktailList.interface';
import closeIcon from '../assets/close-svg.svg';

export const DropdownSearcher: React.FC<{ itemsList: cocktail[] }> = ({
	itemsList,
}) => {
	return (
		<Row justify={'center'} align={'middle'}>
			<Col xs={22} md={20} xl={18}>
				<List
					style={{
						backgroundColor: 'white',
						maxHeight: '300px',
						overflow: 'auto',
					}}
					size="large"
					bordered
					dataSource={itemsList}
					renderItem={({ strDrink, idDrink }) => (
						<Link to={`/${idDrink}`}>
							<List.Item className="list-searcher">
								{strDrink}
							</List.Item>
						</Link>
					)}
					rowKey={({ strDrink }) => strDrink}
				/>
			</Col>
		</Row>

		// </div>
	);
};

export const RandomMocktailSection: React.FC<{ hideRandom: () => void }> = ({
	hideRandom,
}) => {
	const { randomMocktailSelected, getNewRandomMocktail } = useStore(
		(state) => state
	);

	return (
		<Row
			justify={'center'}
			align={'middle'}
			style={{
				backgroundColor: 'white',
				margin: '0 50px 50px 50px',

				borderRadius: '10px',
				padding: '20px',
				position: 'relative',
			}}
		>
			<Button
				className="btn btn-small btn-danger"
				onClick={hideRandom}
				style={{
					position: 'absolute',
					right: '20px',
					top: '20px',
					cursor: 'pointer',
					zIndex: 100,
				}}
			>
				
				<img src={closeIcon} height={'20px'} alt="" />
			</Button>
			<Col xs={24} md={24} xl={12}>
				{/* {JSON.stringify(randomMocktailSelected)} */}
				<Row justify={'center'} align={'middle'}>
					<Col>
						<Title level={3} style={{ textAlign: 'center' }}>
							{randomMocktailSelected.strDrink}
						</Title>
						<Image
							src={randomMocktailSelected.strDrinkThumb}
							height={350}
							preview={false}
						/>
					</Col>
				</Row>
			</Col>

			<Col xs={24} md={24} xl={12}>
				<Row
					style={{
						display: 'flex',
						margin: '20px',

						gap: '20px',
						justifyContent: 'center',
					}}
				>
					<Link to={`/${randomMocktailSelected.idDrink}`}>
						<Button className="btn">View details</Button>
					</Link>
					<Button className="btn" onClick={getNewRandomMocktail}>
						Fetch new random mocktail
					</Button>
				</Row>
			</Col>
		</Row>

		// </div>
	);
};

export const MainSearcher: React.FC = () => {
	const { modifyFullList, mocktailsFullList, getNewRandomMocktail } =
		useStore((state) => state);

	const [searchQuery, setSearchQuery] = useState<string>('');
	const [filteredElements, setFilteredElements] = useState<cocktail[]>([]);
	const [showRandomMocktail, setShowRandomMocktail] =
		useState<boolean>(false);

	const filteredElementsList = (): cocktail[] => {
		return mocktailsFullList.filter((el) =>
			el.strDrink.toLowerCase().includes(searchQuery.toLowerCase())
		);
	};

	const fullSearch = () => {
		setShowRandomMocktail(false);
		setFilteredElements(filteredElementsList);
	};

	const fnShowRandomMocktail = () => {
		getNewRandomMocktail();
		setShowRandomMocktail(!showRandomMocktail);
	};

	const hideRandom = () => {
		setShowRandomMocktail(false);
	};

	useEffect(() => {
		getCocktails().then((data) => {
			modifyFullList(data);
		});
		`
		`;
	}, []);

	useEffect(() => {
		setFilteredElements(filteredElementsList().slice(0, 5));
		setShowRandomMocktail(false);
	}, [searchQuery]);

	return (
		<Row>
			<Col xl={24} sm={24} style={{ minHeight: '100dvh' }}>
				<Row
					justify="center"
					align="middle"
					style={{
						backgroundColor: 'white',
						textDecoration: 'underline',
						textDecorationThickness: '7px',
						textUnderlineOffset: '10px',
						height: '325px',
					}}
				>
					<Col>
						<Row justify={'center'}>
							<Image
								width={180}
								preview={false}
								src="yellow_mocktail.png"
							/>
						</Row>
						<Row style={{ marginTop: '-35px' }}>
							<Typography>
								<span className="page-title">Moockle</span>
							</Typography>
						</Row>
					</Col>
				</Row>
				<Row
					justify="center"
					style={{
						padding: '50px',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						flexDirection: 'row',
						gap: '5px',
					}}
				>
					{/* <Image src=''/> */}
					<Input
						// prefix={<SearchOutlined className="search-icon" />}
						placeholder="Search mocktail..."
						style={{
							textAlign: 'center',
							height: '60px',
							fontSize: '1.6rem',
							fontWeight: 'bold',
							borderRadius: '25px',
							outline: 'none',
							width: '450px',
							// paddingLeft: '80px',
						}}
						onChange={(e) => setSearchQuery(e.target.value)}
						value={searchQuery}
						// onChange={onSearch}
					/>
					<Button className="btn" onClick={() => setSearchQuery('')}>
						Clear search
					</Button>

					<Button
						className="btn"
						onClick={fullSearch}
						disabled={searchQuery.length === 0}
					>
						<svg
							// width="35px"
							// height="35px"
							viewBox="0 0 24 24"
							// fill="none"
							xmlns="http://www.w3.org/2000/svg"
						>
							<path
								d="M16.6725 16.6412L21 21M19 11C19 15.4183 15.4183 19 11 19C6.58172 19 3 15.4183 3 11C3 6.58172 6.58172 3 11 3C15.4183 3 19 6.58172 19 11Z"
								// stroke="white"
								stroke-width="2"
								stroke-linecap="round"
								stroke-linejoin="round"
							/>
						</svg>
						Search
					</Button>
					<Button
						className="btn"
						onClick={fnShowRandomMocktail}
						disabled={showRandomMocktail}
					>
						<svg
							// fill="white"
							// width="35px"
							// height="35px"
							viewBox="0 0 32 32"
							version="1.1"
							xmlns="http://www.w3.org/2000/svg"
						>
							<title>dice-three</title>
							<path d="M27.299 2.246h-22.65c-1.327 0-2.402 1.076-2.402 2.402v22.65c0 1.327 1.076 2.402 2.402 2.402h22.65c1.327 0 2.402-1.076 2.402-2.402v-22.65c0-1.327-1.076-2.402-2.402-2.402zM7.613 27.455c-1.723 0-3.12-1.397-3.12-3.12s1.397-3.12 3.12-3.12 3.12 1.397 3.12 3.12-1.397 3.12-3.12 3.12zM15.974 19.093c-1.723 0-3.12-1.397-3.12-3.12s1.397-3.12 3.12-3.12 3.12 1.397 3.12 3.12-1.397 3.12-3.12 3.12zM24.335 10.732c-1.723 0-3.12-1.397-3.12-3.12s1.397-3.12 3.12-3.12 3.12 1.397 3.12 3.12c-0 1.723-1.397 3.12-3.12 3.12z"></path>
						</svg>
						Random
					</Button>

					{/* <SearchOutlined className="search-icon" /> */}
				</Row>

				{showRandomMocktail && (
					<RandomMocktailSection hideRandom={hideRandom} />
				)}

				{!showRandomMocktail && searchQuery && (
					<Row justify={'center'} align={'middle'}>
						<Col span={24}>
							<DropdownSearcher itemsList={filteredElements} />
						</Col>
						{filteredElements.length > 0 && (
							<Title level={5} style={{ textAlign: 'center' }}>
								Showing {filteredElements.length} results (of a
								full list of {filteredElementsList().length}{' '}
								elements), click on Search button to see the
								full list.
							</Title>
						)}
					</Row>
				)}

				{/* <Row style={{backgroundColor:'red'}}>{JSON.stringify(filteredElements)}</Row>
			<Row>{mocktailsFullList.map(el=><div key={el.idDrink}>{JSON.stringify(el)}</div>)}</Row> */}
			</Col>
		</Row>
	);
};
