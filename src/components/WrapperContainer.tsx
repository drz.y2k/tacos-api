import { Card, Layout, Col, Row,Grid } from 'antd';
const { useBreakpoint } = Grid;
import { PropsWithChildren } from 'react';

const { Header, Content } = Layout;

const WrapperContainer: React.FC<PropsWithChildren> = ({children}) => {

	const screens = useBreakpoint();

	const showMargin=screens.md;

	return (
		<Layout style={{ borderRadius: '10px',margin:showMargin? '0 40px' : 0 }} className="layout">
			<Header className="header">
				<div>Mocktails 🍹</div>
			</Header>
			<Content style={{ padding: '20px' }}>
				{/* <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}> */}
					{/* {foods.map(({ name, image }) => {
						return (
							<Col xs={24} sm={12} md={8} lg={4} key={name}>
								<Card
									style={{
										height: '100%',
									}}
									hoverable
									cover={
										<Link to={`/${name.toLowerCase()}`}>
											<div
												style={{
													overflow: 'hidden',
													height: '100px',
													objectFit: 'cover',
													backgroundImage: `url(${image})`,
													backgroundPosition:
														'center',
													backgroundSize: 'cover',
													borderRadius: '10px',
												}}
											></div>
										</Link>
									}
								>
									<Card.Meta title={name} />
								</Card>
							</Col>
						);
					})} */}
					{children}
				{/* </Row> */}
			</Content>
		</Layout>
	);
};

export default WrapperContainer;