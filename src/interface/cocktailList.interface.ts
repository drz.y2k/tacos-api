export interface cocktail {
	strDrink: string;
	strDrinkThumb: string;
	idDrink: string;
}