import apiClient from '../config/axios';

interface cocktail {
	strDrink: string;
	strDrinkThumb: string;
	idDrink: string;
}

export const getCocktails = async () => {
	const response = await apiClient.get(`filter.php?a=Non_Alcoholic`, {
		// headers: {
		// 	'Content-Type': 'application/json',
		// 	Authorization: `Bearer ${token}`,
		// },
	});
	return response.data.drinks;
};


export const getCocktail = async (id: string) => {
	const response = await apiClient.get(`lookup.php?i=${id}`, {
		// headers: {
		// 	'Content-Type': 'application/json',
		// 	Authorization: `Bearer ${token}`,
		// },
	});
	return response.data.drinks[0];
};
