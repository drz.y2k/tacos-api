import { create } from 'zustand';
import { cocktail as cocktailList } from '../interface/cocktailList.interface';
import { cocktail } from '../interface/cocktail.interface';

interface StoreState {
	randomsMocktailsList: cocktail[];
	singleMocktailSelected: cocktailList;
	mocktailsFullList: cocktailList[];
	// modifyRandomList: (newList: any) => void;
	modifyFullList: (newList: any) => void;
	modifySelectedItem: (item: any) => void;
	getRandomMocktail: (hide?: number) => void;
	activatedRandomMocktail: boolean;

	getNewRandomMocktail: () => void;
	randomMocktailSelected: cocktailList;
	// increment: () => void;
	// decrement: () => void;
	// reset: () => void;
}

// Creamos nuestro store con el estado inicial y las acciones
export const useStore = create<StoreState>((set, get) => ({
	// Estado inicial
	randomsMocktailsList: [],
	singleMocktailSelected: {} as cocktailList,
	mocktailsFullList: [],
	activatedRandomMocktail: false,
	randomMocktailSelected: {} as cocktailList,
	// count:0,
	// Acciones
	// modifyRandomList: (newList: any) =>
	// 	set((state: any) => ({ randomsMocktailsList: newList })),

	modifyFullList: (newList: any) =>
		set((state: any) => ({ mocktailsFullList: newList })),

	modifySelectedItem: (item: any) =>
		set((state: any) => ({ singleMocktailSelected: item })),

	getNewRandomMocktail: () =>{

		let randomIndex= Math.floor(Math.random() * get().mocktailsFullList.length) + 1;

		set((state: any) => ({
			randomMocktailSelected: get().mocktailsFullList[randomIndex],
		}))

	},

	getRandomMocktail: (hide = 1) => {
		if (hide == 1) {
			set({ activatedRandomMocktail: !get().activatedRandomMocktail });
		}

		function returnRandomInt() {
			const indexMocktail =
				Math.floor(Math.random() * get().mocktailsFullList.length) + 1;
			return indexMocktail;
		}

		let i = 0;

		function cicloRecursivo() {
			i++;
			if (i > 10) return;
			set((state: any) => ({
				singleMocktailSelected:
					get().mocktailsFullList[returnRandomInt()],
			}));
			// changeRandomMocktail(mocktailsList[returnRandomInt()]);
			setTimeout(function () {
				cicloRecursivo();
			}, 65);
		}
		if (hide == 1) {
			setTimeout(() => {
				cicloRecursivo();
			}, 300);
		} else {
			cicloRecursivo();
		}
	},
	// increment: () => set((state: any) => ({ count: state.count + 1 })),
	// decrement: () => set((state: any) => ({ count: state.count - 1 })),
	// reset: () => set({ count: 0 }),
}));
