import { Home } from './components/Home';
import { MocktailDetail } from './temp/MocktailDetail';
import { MainSearcher } from './components/MainSearcher';
import WrapperContainer from './components/WrapperContainer';
import { MocktailsMain } from './components/MocktailMain';
import { useRoutes } from 'react-router-dom';
import './App.scss';

function App() {
	let element = useRoutes([
		{
			path: '/',
			element: <MainSearcher />,
			// element: <Home />
		},
		{
			path: '/:idCocktail',
			element: <MocktailsMain />,
			children: [
				{
					path: ':variant',
					element: <MocktailDetail />,
				},
			],
		},
	]);

	// return <WrapperContainer>{element}</WrapperContainer>;
	return <>{element}</>;
}

export default App;
